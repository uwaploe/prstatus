// Prstate stores the most recent DP vehicle status information to
// a hash-table in a Redis data store.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"bitbucket.org/uwaploe/go-dputil"
	"github.com/garyburd/redigo/redis"
)

var Version = "dev"
var BuildDate = "unknown"

type Record struct {
	source string
	data   map[string]interface{}
}

// Reformat a profiler data record
func convert_profiler(data map[string]interface{}) {
	var scaled = map[string]float64{
		"pressure":      0.001,
		"voltage":       0.001,
		"current":       0.001,
		"energy":        0.001,
		"motor_current": 0.001,
		"vmon":          0.001,
		"humidity":      0.1,
		"rel_charge":    1.0,
	}

	// Convert all scaled integers to floats
	for k, v := range scaled {
		if _, ok := data[k]; ok {
			data[k] = data[k].(float64) * v
		}
	}

	// Make sure the profile number is a signed integer
	data["profile"] = int64(data["profile"].(float64))

	// Expand the list of internal temperature values
	for i, val := range data["itemp"].([]interface{}) {
		data[fmt.Sprintf("itemp_%d", i)] = val.(float64) * float64(0.1)
	}

	// Delete the list.
	delete(data, "itemp")
}

// Reformat a DSC data record
func convert_dock(data map[string]interface{}) {
	var scaled = map[string]float64{
		"T_ambient":  0.001,
		"T_heatsink": 0.001,
		"humidity":   0.1,
		"v12":        0.001,
	}

	// Convert all scaled integers to floats
	for k, v := range scaled {
		if _, ok := data[k]; ok {
			data[k] = data[k].(float64) * v
		}
	}

	data["ips_status"] = int64(data["ips_status"].(float64))
}

// Convert all data record fields to integers
func convert_to_int(data map[string]interface{}) {
	for k, v := range data {
		data[k] = int64(v.(float64))
	}
}

// Reformat each data record according to its source
func reformat_data(dr *dputil.DataRecord) {
	switch dr.Source {
	case "profiler":
		convert_profiler(dr.Data.(map[string]interface{}))
	case "dock":
		convert_dock(dr.Data.(map[string]interface{}))
	case "flntu_1":
		convert_to_int(dr.Data.(map[string]interface{}))
	case "flcd_1":
		convert_to_int(dr.Data.(map[string]interface{}))
	}
}

// Monitor a Redis pub-sub channel for profiler messages and write the
// data section to a Go channel.
func data_reader(psc redis.PubSubConn) <-chan Record {
	c := make(chan Record, 1)
	go func() {
		defer close(c)
		dr := dputil.DataRecord{}
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				}
			case redis.Message:
				err := json.Unmarshal(msg.Data, &dr)
				if err == nil {
					data := dr.Data.(map[string]interface{})
					reformat_data(&dr)
					data["timestamp"] = dr.T.Unix()
					c <- Record{source: dr.Source, data: data}
				} else {
					log.Printf("JSON decode error: %v", err)
				}

			}
		}
	}()

	return c
}

var usage = `
Usage: prstate [options]

Write the latest profiler state information to a Redis hash-tables with
a key corresponding to the data source with "dp:" prepended
`

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	showvers := flag.Bool("version", false,
		"Show program version information and exit")
	dchan := flag.String("chan", "data.eng", "Name of Redis pubsub channel")

	flag.Parse()
	if *showvers {
		fmt.Printf("%s %s\n", os.Args[0], Version)
		fmt.Printf("  Build date: %s\n", BuildDate)
		fmt.Printf("  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	// Open the pub-sub connection
	psconn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		log.Fatalln(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	// Open the standard connection
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		log.Fatalln(err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	// Start the data reader and subscribe to the data channel
	c := data_reader(psc)
	psc.Subscribe(*dchan)

	log.Println("Waiting for data records")
	for rec := range c {
		conn.Send("MULTI")
		for k, v := range rec.data {
			conn.Send("HSET", "dp:"+rec.source, k, v)
		}
		_, err = conn.Do("EXEC")
		if err != nil {
			log.Println(err)
		}
	}
}
