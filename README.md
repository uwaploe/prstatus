# Profiler Status Monitor

This program is run on the Docking Station Controller (DSC). It subscribes
to the Redis pub-sub channel *data.eng* which broadcasts the profiler
status records and writes the most recent status message to a hash-table
in the Redis data store.
